package org.usfirst.frc253.Code2016.commands;

import org.usfirst.frc253.Code2016.Robot;

import java.util.*;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class OpenPorky extends Command {

    public OpenPorky() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.liftSystem);
    	requires(Robot.drivetraintank);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	long start = System.currentTimeMillis();
    	long endTime = start + 3000;
    	while(System.currentTimeMillis() < endTime){
    		Robot.drivetraintank.setLeft_Back(1.0);
        	Robot.drivetraintank.setLeft(1.0);
        	Robot.drivetraintank.setRight(1.0);
        	Robot.drivetraintank.setRight_Back(1.0);	
    	}
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
